#default_params =
#  : true
#  : true
##  shaders: true
#  : false
#  : false
#  : true
#  : false


# name, [false value, true values]
checkbox_commands =
    screen: ['--window', '--fullscreen']
    shadows: ['--shadows', '--noshadow']
    shaders: ['', '--noshaders']
    help: ['', '--help']
    version: ['', '--version']
    sound: ['--sound', '--nosound']
    cheat: ['', '--cheat']




#checkboxes = ['screen','shadows','help','version','sound','cheat']
checkboxes = ['screen','help','version','sound']

settings = $('#settings')

add_element =  (name) ->
  val = localStorage[name]
  input = $('<input>').attr({type:'checkbox', id: "game_#{name}"})
  if val == '1'
    input.attr('checked', true)

  wrapper = $("<div><label style='text-transform:capitalize'>#{name}</label></div>")
  wrapper.append(input)
  settings.append(wrapper)
  input.change (evt) ->
    localStorage[name] = $(this).is(':checked') and 1 or 0
    calculate_string()


get_params = () ->
  (checkbox_commands[name][parseInt localStorage[name]] for name in checkboxes).filter((x) -> x != '').concat localStorage.additionalArgs.split(' ')
global.get_params = get_params

calculate_string = () ->
  command_line = $('#command_line')
  command_line.text get_params().join(' ')


parseAdditionalArgs = (args) ->
  args = args.split ' '
  for arg in args
    if arg.indexOf('--') != 0
      return false
  return args


$(document).ready ->
  console.log 'x222'
  add_element name for name in checkboxes
  calculate_string()
  $('#additional_args').val(localStorage.additionalArgs).change (evt) ->
    localStorage.additionalArgs = $(this).val()
    calculate_string()








#--savegame NAME 	Load a saved game NAME
#--resolution WIDTHxHEIGHT 	Set the dimensions of the viewport (screen or window)
#Debugging
#--debug FLAG 	Show debug for FLAG (Valid flags are: "all", "main", "sound", "video", "wz", "3d", "texture", "net", "memory", "error", "script"). Every flag must be preceded by a new --debug. "all" includes every other option.
#--debugfile FILE 	Log debug output in FILE