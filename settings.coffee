localStorage.path_to_wz_executable = localStorage.path_to_wz_executable || ''
localStorage.path = localStorage.path || ''
localStorage.pre_script_exe = localStorage.pre_script_exe || ''
localStorage.pre_script = localStorage.pre_script || ''


$(document).ready ->
    console.log 'x1111'
    file_path = $('#path_to_wz')
    file_path.text localStorage.path_to_wz_executable
    file_dialog =  $('input#path_to_wz_file_dialog')
    file_path.click  (evt) -> file_dialog.trigger('click')
    file_dialog.change  (evt) ->
        val = $(this).val()
        file_path.text val
        localStorage.path_to_wz_executable = val
        localStorage.path = get_path(val)

    pre_script = $('#pre_script')
    pre_script.val localStorage.pre_script
    pre_script.change (evt) ->
      localStorage.pre_script = $(this).val()

    pre_script_exe = $('#pre_script')
    pre_script_exe.val localStorage.pre_script_exe
    pre_script_exe.change (evt) ->
      localStorage.pre_script = $(this).val()