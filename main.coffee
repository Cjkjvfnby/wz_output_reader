spawn  = require('child_process').spawn


global.stop_game = () ->
  global.game_process.kill()
  global.game_process = null

global.run_game = () ->
  window.log_storage = []
  #ps = spawn('c:/python27/python.exe', ['G:/projects/nullbot/make.py'])
  console.log 'zzzz', localStorage.pre_script and localStorage.pre_script_exe

  if localStorage.pre_script and localStorage.pre_script_exe
    ps = spawn(localStorage.pre_script_exe, [localStorage.pre_script])
    ps.stderr.on 'end',  _run_game
    ps.stderr.on 'data', (data) ->
      text = data.toString()
      console.log("error:", text)
  else
    _run_game()

stderr = $('#stderr_log')
info = /^(\w*) *\|(\d{2}:\d{2}:\d{2}): \[([\w:]*)\] (.*)/
spam1 = /last message repeated/

convert_to_html = (arg_list) ->
  debugger_name = arg_list[1]
  time = arg_list[2]
  script_name = arg_list[3]
  script_message = arg_list[4]
  "<div class='debug_#{debugger_name}' title='#{debugger_name}'> <div class='time'>#{time}</div> <div class='script_name'>#{script_name}</div> #{script_message}</div>"


default_html = (str) ->
  str = str.replace(/^"/, '').replace(/"$/, '')
  window.log_storage.push(str)
  "<div class='debug_default'>#{str}</div>"

handle_string = (val) ->
  val = val.replace('\r', '').trim()
  if spam1.test(val)  then return
  if info.test(val) then stderr.before(convert_to_html(info.exec(val)))
  else
    stderr.before(default_html(val))


_add_text = (data) ->
  text = data.toString()
  new_text = stderr.text() + text
  lines = new_text.split('\n').reverse()
  stderr.text(lines.shift())
  lines.reverse()
  (handle_string(x) for x in lines)


_run_game = (text) ->
  cmd_args = global.get_params()
  ps = spawn(localStorage.path_to_wz_executable, cmd_args)
  ps.stderr.on 'data', (data) ->
    _add_text(data, stderr)
  ps.stdout.on 'data', (data) ->
    _add_text(data, stderr)

  ps.stdout.on 'end', (data) ->
    $("#base_launcher").find('b').toggle()
    global.game_process = null

  global.game_process = ps

