fs = require('fs')
global.game_process = null
is_wz = (name) ->
  arr = name.split('.')
  if arr[1] == 'wz' and arr[0] not in ['base', 'mp']
    true
  else
    false

get_path = (path) ->
  res = path.split('\\')
  res[0..res.length-2].join('\\')

$(document).ready ->
  $("#base_launcher").click (event) ->
    if global.game_process
      global.stop_game()
    else
      $("#base_launcher").find('b').toggle()
      global.game_process = global.run_game()
      event.preventDefault()

  $('#clear_log').click  (event) ->
        $('#stderr_log').html('')
        $('#stdout_log').html('')
        event.preventDefault()

  $('.log_panel').click (event) ->
        targert = $(event.target)
        if targert.attr('class') == 'object_header'
            targert.next().toggle()

  $('#filter_info').click (event) ->
      $('.debug_info').toggle()
      $('#filter_info').toggleClass('pressed')


  $('#filter_script').click (event) ->
      $('.debug_script').toggle()
      $('#filter_script').toggleClass('pressed')


  $('#filter_error').click (event) ->
      $('.debug_error').toggle()
      $('#filter_error').toggleClass('pressed')


  $('#filter_default').click (event) ->
    $('.debug_default').toggle()
    $('#filter_default').toggleClass('pressed')


