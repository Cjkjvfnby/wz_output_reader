import os
from datetime import date
from zipfile import ZipFile

current_path = os.path.dirname(__file__)
package_name = 'wzout_%s.nw' % date.today()

exclude_dirs = [os.path.join(current_path, x) for x in ['.git', '.idea']]
exclude_files = ['make.py', '.gitignore', 'README', 'package.json', 'deploy_package.json']

result_files = []
for base, folders, files in os.walk(current_path):
    if any([base.startswith(dir) for dir in exclude_dirs]):
        continue
    result_files.extend([os.path.join(base, x) for x in files if x not in exclude_files and not x.endswith('.nw')])

with ZipFile(package_name, 'w') as myzip:
    for file in  result_files:
        file = file.replace('\\', '/').replace(current_path + '/', '')
        print file
        myzip.write(file)
    myzip.write('deploy_package.json', arcname='package.json')




